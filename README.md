# incidents

Web service to display and map incident reports from the [Pacifica police blotter](http://www.cityofpacifica.org/depts/police/media/media_bulletin.asp).

[See it](http://pacifica-incidents.herokuapp.com/). Note: because it՚s hosted on Heroku՚s free tier, it now can take up to 20 seconds to start up after being idle. 

## Usage

Start server:
> lein ring server

## License

Copyright © 2014-2015 Concerned Hackers of Pacifica

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
